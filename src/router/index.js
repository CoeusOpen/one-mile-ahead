import Vue from 'vue';
import Router from 'vue-router';
import Main from '@/components/Main';
import Counties from '@/components/Counties';
import County from '@/components/County';
import Landmark from '@/components/Landmark';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
    },
    {
      path: '/counties',
      name: 'Counties',
      component: Counties,
    },
    {
      path: '/county/:county',
      name: 'County',
      component: County,
    },
    {
      path: '/Landmark/:id',
      name: 'Landmark',
      component: Landmark,
      props: { details: {} },
    },
  ],
});

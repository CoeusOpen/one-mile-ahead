import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(
  VueAxios,
  axios,
);

// import markerStore from '@/store/markers_lite-trunc.json';

const apiRoute = process.env.API_ROUTE;

const api = {
  // hasLocalDatabase: () => typeof markerStore !== 'undefined',
  // loadLocalDatabase: () => markerStore.RECORDS,
  // getAllLandmarks: () => markerStore.RECORDS,
  // getLandmarkById: id => markerStore.RECORDS.find(
  //   // TODO: remove parseInt() when data is scrubbed clean
  //   record => parseInt(record.markernum, 10) === id,
  // ),
  getAllLandmarks: () => Vue.axios.get(`${apiRoute}/landmark`),
  getLandmarkById: id => Vue.axios.get(`${apiRoute}/landmark/${id}`),
  getAllCounties: () => Vue.axios.get(`${apiRoute}/county`),
  getMarkersInCounty: county => Vue.axios.get(`${apiRoute}/county/${county}`),
};

export default api;

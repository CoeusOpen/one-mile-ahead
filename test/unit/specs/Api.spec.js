import api from '@/api';
import markerStore from '@/store/markers_lite-trunc.json';

describe('API Module', () => {
  const landmarkId = 10;
  it('should have a method check for local JSON files', () => {
    expect(api.hasLocalDatabase()).toBeDefined();
  });

  it('should have a method for retrieving records from local JSON files', () => {
    expect(api.loadLocalDatabase()).toBeDefined();
  });

  it('should load an object with at least one valid record', () => {
    const records = api.loadLocalDatabase();

    expect(records.length).toBeGreaterThan(0);
    expect(records[0]).toHaveProperty('markernum');
    expect(records[0]).toHaveProperty('title');
    expect(records[0]).toHaveProperty('markertext');
  });

  it('should be able to retrieve a marker by ID (markernum)', () => {
    expect(api.getLandmarkById(landmarkId)).toBeDefined();
  });

  it('should be able to retrieve all available records', () => {
    expect(api.getAllLandmarks().length).toEqual(markerStore.RECORDS.length);
  });
});

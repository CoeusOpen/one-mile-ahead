import Vue from 'vue';
import Landmark from '@/components/Landmark';
import api from '@/api';


describe('Landmark.vue', () => {
  const data = { details: {
    markernum: '1', // TODO: change to int when data is scrubbed clean
    title: 'Early Community Building',
    markertext: 'Built 1877 by Charles Holman, builder-carpenter from Sweden. Stone was quarried south of town. Over the years, structure housed a school, churches, a newspaper office and a community center.\n  It was purchased by J. E. McClelen in 1949 and restored as a private home. \n\nRecorded Texas Historic Landmark - 1969.',
  } };
  const Constructor = Vue.extend(Landmark);
  const vm = new Constructor({ propsData: data }).$mount();

  it('should display the first landmark title', () => {
    expect(vm.$el.querySelector('.title').innerHTML)
    .toEqual(data.details.title);
  });

  it('should have an ID prop', () => {
    // TODO: remove parseInt() when data is scrubbed clean
    expect(parseInt(vm.details.markernum, 10)).toEqual(parseInt(data.details.markernum, 10));
  });

  it('should retrieve a marker by ID from the API', () => {
    expect(api.getLandmarkById(1)).toEqual(data.details);
  });
});

import Vue from 'vue';
import Main from '@/components/Main';

describe('Main.vue', () => {
  const Constructor = Vue.extend(Main);
  const vm = new Constructor().$mount();

  it('should load the main page', () => {
    expect(vm.$el.querySelector('.greeting').textContent)
    .toEqual('One Mile Ahead');
  });
});

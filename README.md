# README #

### What is *One Mile Ahead*? ###

Texas has a lot of rich and compelling history. Many of its stories are cast in aluminum and permanently positioned alongside the thousands of miles of roads that stretch across it. Though care has been taken to notify motorists of approaching installations, the signs are frequently bypassed. 
With over 16-thousand markers existing today, and hundreds more added each year, the One Mile Ahead web application seeks to bring history into the back seat of every family vacation, and inspire the next generation of Texans.

* Version: 0.0.1

### How do I get set up? ###

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact